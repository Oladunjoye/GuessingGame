<!DOCTYPE html>
<html>
<head>
	<title>Leap Year</title>
</head>
<style type="text/css">
body{
	color : #fff;
	background-color: #222;
	font-size: 20px;
	
}
span{
	color: red;
}
</style>
<body>
	<div>
	<?php
  $numberOfLeapYear = 0;
  for ($year = 1980; $year <= 2018 ; $year++){
   if ($year% 4 == 0){
     print "$year <span>Leap year</span>  <br>";
     $numberOfLeapYear++;

   }
   else {
     print "$year <br>";
   }
 }

   echo "The number of leap years is $numberOfLeapYear";
  ?>
</div>

</body>
</html>